import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Article } from '../models/articles.model';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  loading: boolean = false;
  articles: Article[] = [];
  article!: Article;
  

  constructor(public http: HttpClient) { }

  ngOnInit(): void {
    this.loadArticles();
    }

  loadArticles(): void {
    this.loading = true;
    this.http.get<Article[]>('https://jsonplaceholder.typicode.com/posts').subscribe(res => {
      this.articles = res;
      this.loading = false;
    });
  }

  loadArticleById(id: string): void {
    this.loading = true;
    this.http.get<Article>('https://jsonplaceholder.typicode.com/posts/' + id).subscribe(res => {
      this.article = res;
      this.loading = false;
    });
  }
  
}

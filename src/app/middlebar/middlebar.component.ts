import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-middlebar',
  templateUrl: './middlebar.component.html',
  styleUrls: ['./middlebar.component.css']
})
export class MiddlebarComponent implements OnInit {

  lista: string[] = ["prova 1", "prova 2", "prova 3", "prova 4"];

  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  today: Date;

  show: boolean = false;

  color: string = "black";

  constructor() {
    this.today = new Date();
  }

  ngOnInit(): void {
  }

  showHidden(): void {
    this.show = !this.show;
  }

  hello(field: HTMLSelectElement) {
    if(field.value == "1"){
      this.color = "red";
    }else if(field.value == "2"){
      this.color = "green";
    }else {
      this.color = "blue";
    }
  }
}

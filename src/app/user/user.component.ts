import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/users.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  loading: boolean = false;

  users: User[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.loadUser();
  }


  loadUser(): void {
    this.loading = true;
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users').subscribe(res => {
      this.users = res;
      this.loading = false;
    });
  }
}
